package storage

import (
	"io"
	"os"
	"path"

	"gitlab.com/gevleeog/storage-proxy/config"
	"gitlab.com/gevleeog/storage-proxy/database/model"
)

type localStorage struct {
}

func newLocalStorage() FileStorage {
	return &localStorage{}
}

func (s *localStorage) SaveFile(data *model.FileData, r io.Reader) error {
	dirPath := s.getDirPathFromScope(data.Scope)
	err := s.ensureDir(dirPath)
	if err != nil {
		return err
	}

	file, err := os.Create(path.Join(dirPath, data.ID))
	if err != nil {
		return err
	}

	_, err = io.Copy(file, r)
	return err
}

func (s *localStorage) GetFile(data *model.FileData) (io.Reader, error) {
	return os.Open(path.Join(s.getDirPathFromScope(data.Scope), data.ID))
}

func (s *localStorage) getDirPathFromScope(scope string) string {

	return path.Join(config.GetString("storage.local.root"), scope)
}

func (s *localStorage) ensureDir(dir string) error {
	_, err := os.Stat(dir)

	if err == nil {
		return err
	}

	if os.IsNotExist(err) {
		err := os.MkdirAll(dir, os.ModePerm)
		return err
	}

	return nil
}
