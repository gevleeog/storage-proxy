package storage

import (
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	model "gitlab.com/gevleeog/storage-proxy/database/model"
)

func TestFileStorageFacade(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockObj := NewMockFileStorage(mockCtrl)
	facade := &fileStorageFacade{
		StorageFactory: func(s string) FileStorage {
			return mockObj
		},
	}

	mockObj.EXPECT().SaveFile(gomock.Any(), gomock.Any()).MinTimes(1).MaxTimes(1)

	facade.SaveFile(&model.FileData{}, (*os.File)(nil))
}
