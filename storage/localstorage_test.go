package storage

import (
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gevleeog/storage-proxy/config"
	model "gitlab.com/gevleeog/storage-proxy/database/model"
)

func setup(t *testing.T) func(*testing.T) {
	var cfg = []byte(`
storage:
  local:
    root: ./test_root
`)
	config.InitFromBytes(cfg)

	return func(t *testing.T) {
		os.RemoveAll("./test_root/")
	}
}

func TestSaveFile(t *testing.T) {
	teardown := setup(t)
	defer teardown(t)
	testContent := "test content"
	err := newLocalStorage().SaveFile(&model.FileData{
		ID:    "qwerty",
		Scope: "scp",
	}, strings.NewReader(testContent))

	assert.NoError(t, err)
	assert.FileExists(t, "./test_root/scp/qwerty")
	assert.True(t, contentEqual("./test_root/scp/qwerty", []byte(testContent)))
}

func contentEqual(path string, content []byte) bool {
	b, err := ioutil.ReadFile(path)
	if err == nil && string(b) == string(content) {
		return true
	}

	return false
}
