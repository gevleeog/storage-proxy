package config

import (
	"bytes"
	"fmt"
	"os"

	"github.com/spf13/viper"
)

func Init(configPath string) {
	viper.SetConfigType("yaml")
	viper.SetConfigFile(configPath)
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("Can't read config:", err)
		os.Exit(1)
	}
}

func InitFromBytes(data []byte) {
	viper.SetConfigType("yaml")
	if err := viper.ReadConfig(bytes.NewBuffer(data)); err != nil {
		fmt.Println("Can't read config:", err)
		os.Exit(1)
	}
}

func GetString(key string) string {
	return viper.GetString(key)
}
