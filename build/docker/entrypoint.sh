#!/usr/bin/env bash
cd /go/src/gitlab.com/gevleeog/storage-proxy
sh ./build/linux/before-build.sh
sh ./build/linux/build-server-$PLATFORM.sh