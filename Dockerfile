FROM alpine

COPY bin/linux386/server /app/server
COPY scripts/docker /
RUN mkdir /storage-proxy-scope
RUN chmod +x /entrypoint.sh
VOLUME /storage-proxy-scope

WORKDIR /app

ENTRYPOINT ["/entrypoint.sh"]
