package database

import (
	"gitlab.com/gevleeog/storage-proxy/database/model"
)

type Store interface {
	FilesDataStore() FilesDataStore
}

type FilesDataStore interface {
	GetFileDataById(id string) (*model.FileData, error)
	SaveFileData(*model.FileData) error
}
