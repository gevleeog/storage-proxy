package mongo

import (
	"testing"

	"gopkg.in/mgo.v2"

	"gitlab.com/gevleeog/storage-proxy/config"
)

func setup(t *testing.T) func(t *testing.T) {
	t.Log("Init config")
	config.Init("db-mongo-tests-config.yml")
	return func(t *testing.T) {
		t.Log("Removing test database...")
		err := Database(func(db *mgo.Database) error {
			return db.DropDatabase()
		})

		if err != nil {
			t.Log("Cannot drop test database", err)
		} else {
			t.Log("Done.")
		}
	}
}

func StoreTest(t *testing.T, name string, f func(*testing.T)) {
	tearDown := setup(t)
	defer tearDown(t)
	t.Run(name, f)
}
