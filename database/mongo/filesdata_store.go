package mongo

import (
	"gitlab.com/gevleeog/storage-proxy/database"
	"gitlab.com/gevleeog/storage-proxy/database/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type MongoFileDataStore struct {
	CollectionName string
}

func NewMongoFileDataStore() database.FilesDataStore {
	return &MongoFileDataStore{
		CollectionName: "filedata",
	}
}

func (store *MongoFileDataStore) GetFileDataById(id string) (*model.FileData, error) {
	data := model.FileData{}
	err := Collection(store.CollectionName, func(c *mgo.Collection) error {
		return c.Find(bson.M{"_id": id}).One(&data)
	})

	if err == mgo.ErrNotFound {
		err = database.ErrNotFound
	}

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func (store *MongoFileDataStore) SaveFileData(data *model.FileData) error {
	return Collection(store.CollectionName, func(c *mgo.Collection) error {
		data.ID = bson.NewObjectId().Hex()
		return c.Insert(data)
	})
}
