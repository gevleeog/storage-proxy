package mongo

import (
	"gitlab.com/gevleeog/storage-proxy/database"
)

type MongoStore struct {
}

func NewMongoStore() database.Store {
	return &MongoStore{}
}

func (store *MongoStore) FilesDataStore() database.FilesDataStore {
	fileDataStore := NewMongoFileDataStore()
	return fileDataStore
}
