package mongo

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gevleeog/storage-proxy/database/model"

	"gopkg.in/mgo.v2"
)

func TestFind(t *testing.T) {
	StoreTest(t, "FileDataStore_TestFind", func(t *testing.T) {
		testFileData := &model.FileData{
			ID:    "test",
			Name:  "test",
			Scope: "test",
		}
		Collection("filedata", func(c *mgo.Collection) error {

			c.Insert(testFileData)
			store := NewMongoFileDataStore()
			result, err := store.GetFileDataById("test")

			assert.Equal(t, &testFileData, &result)

			return err
		})
	})
}
