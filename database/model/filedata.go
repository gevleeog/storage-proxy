package model

type FileData struct {
	ID          string `json:"id" bson:"_id,omitempty"`
	Name        string `json:"name" bson:"name,omitempty"`
	Scope       string `json:"scope" bson:"scope,omitempty"`
	StorageType string `json:"storage_type" bson:"storage_type,omitempty"`
}
