package http

import (
	"net/http"

	"github.com/gorilla/mux"

	"github.com/sirupsen/logrus"
)

type Context struct {
	UnhandledError error
	Error          *Error
	Vars           map[string]string
}

type Error struct {
	Msg string
}

type handler struct {
	handleFunc func(*Context, http.ResponseWriter, *http.Request)
}

func NewApiError(msg string) *Error {
	return &Error{
		Msg: msg,
	}
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	context := Context{
		Vars: mux.Vars(r),
	}
	h.handleFunc(&context, w, r)
	if context.UnhandledError != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.WithError(context.UnhandledError).Error("REQUEST ERROR")
		WriteJson(w, NewApiError("Internal error. Please contact with administrator."))
	}
}

func (api *Api) Default(f func(*Context, http.ResponseWriter, *http.Request)) http.Handler {
	return &handler{
		handleFunc: f,
	}
}
