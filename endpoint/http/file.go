package http

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"

	"gitlab.com/gevleeog/storage-proxy/database"
	"gitlab.com/gevleeog/storage-proxy/storage"

	"gitlab.com/gevleeog/storage-proxy/database/model"
	"gitlab.com/gevleeog/storage-proxy/database/mongo"
)

func (api *Api) InitFile() {
	fileRouter := api.Routes.ApiRoot.PathPrefix("/file").Subrouter()
	fileRouter.Handle("/{file_id}", api.Default(getFile)).Methods("GET")
	fileRouter.Handle("", api.Default(saveDefinition)).Methods("POST")
	fileRouter.Handle("/upload/{file_id}", api.Default(uploadFile)).Methods("POST")
	fileRouter.Handle("/download/{file_id}", api.Default(downloadFile)).Methods("GET")
}

func getFile(c *Context, w http.ResponseWriter, r *http.Request) {
	file_id := c.Vars["file_id"]

	store := mongo.NewMongoStore()

	data, err := store.FilesDataStore().GetFileDataById(file_id)

	if err != nil {
		if err == database.ErrNotFound {
			http.NotFound(w, r)
		} else {
			WriteJson(w, NewApiError("Someting went wrong"))
			w.WriteHeader(http.StatusBadRequest)
		}
	} else {
		WriteJson(w, data)
	}
}

func uploadFile(c *Context, w http.ResponseWriter, r *http.Request) {
	fileID := c.Vars["file_id"]

	store := mongo.NewMongoStore()

	data, err := store.FilesDataStore().GetFileDataById(fileID)

	if err != nil {
		if err == database.ErrNotFound {
			http.NotFound(w, r)
		} else {
			WriteJson(w, NewApiError("Someting went wrong"))
			w.WriteHeader(http.StatusBadRequest)
		}
	} else {
		fileStorage := storage.NewFileStorage()
		err := fileStorage.SaveFile(data, r.Body)

		if err != nil {
			logrus.WithError(err).Error(data)
			WriteJson(w, NewApiError("Someting went wrong"))
			w.WriteHeader(http.StatusBadRequest)
		}
	}
}

func downloadFile(c *Context, w http.ResponseWriter, r *http.Request) {
	fileID := c.Vars["file_id"]

	store := mongo.NewMongoStore()

	data, err := store.FilesDataStore().GetFileDataById(fileID)

	if err != nil {
		if err == database.ErrNotFound {
			http.NotFound(w, r)
		} else {
			WriteJson(w, NewApiError("Someting went wrong"))
			w.WriteHeader(http.StatusBadRequest)
		}
	} else {
		fileStorage := storage.NewFileStorage()
		reader, err := fileStorage.GetFile(data)

		if err != nil {
			logrus.WithError(err).Error(data)
			WriteJson(w, NewApiError("Someting went wrong"))
			w.WriteHeader(http.StatusBadRequest)
		} else {
			io.Copy(w, reader)
		}
	}
}

func saveDefinition(c *Context, w http.ResponseWriter, r *http.Request) {
	store := mongo.NewMongoStore()

	data := &model.FileData{}

	GetFromJson(r.Body, data)

	err := store.FilesDataStore().SaveFileData(data)

	if err != nil {
		logrus.WithError(err).Error(data)
		WriteJson(w, NewApiError("Someting went wrong"))
		w.WriteHeader(http.StatusBadRequest)
	}

	result := &struct {
		ID string `json:"id"`
	}{
		data.ID,
	}

	WriteJson(w, result)
}
