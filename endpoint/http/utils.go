package http

import (
	"encoding/json"
	"io"
	"net/http"
)

func WriteJson(w http.ResponseWriter, data interface{}) error {
	return json.NewEncoder(w).Encode(data)
}

func GetFromJson(r io.Reader, data interface{}) error {
	return json.NewDecoder(r).Decode(data)
}
